import React from 'react';

class PresentationForm extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            presenter_name: '',
            presenter_email:'',
            company_name:'',
            title:'',
            synopsis:'',
            conferences:[]
          };
          this.handleFieldChange = this.handleFieldChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.conferences;
        console.log(data);
        const conferenceId = data.conference

        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        console.log(presentationUrl)
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        
        
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
        const newPresentation = await response.json();
        console.log(newPresentation);

        const cleared = {
            presenter_name: '',
            presenter_email:'',
            company_name:'',
            title:'',
            synopsis:'',
            conference:''
          };
        this.setState(cleared);
        
    

    }
}

handleFieldChange(event) {
    const value = event.target.value;
    this.setState({[event.target.id]: value})
}


async componentDidMount() {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        this.setState({conferences: data.conferences});

        
       
    }
  }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={this.handleSubmit} id="create-presentation-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFieldChange} value={this.state.presenter_name} placeholder="Name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                    <label htmlFor="presnter_name">Presenter name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFieldChange} value={this.state.presenter_email} placeholder="email" required type="email" name ="presenter_email" id="presenter_email" className="form-control" />
                    <label htmlFor="presenter_email">Presenter email</label>
                  </div>
                  
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFieldChange} value={this.state.company_name} placeholder="company_name" required type="text" name="company_name" id="company_name" className="form-control" />
                    <label htmlFor="company_name">Company name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFieldChange} value={this.state.title} placeholder="title" required type="text" name="title" id="title" className="form-control" />
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className="form-floating mb-3">
                    <textarea onChange={this.handleFieldChange} value={this.state.synopsis} placeholder="synopsis" className="form-control" id="synopsis" name="synopsis" id="synopsis" rows="3"></textarea>
                    <label htmlFor="synopsis">Synopsis</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleFieldChange} value={this.state.conference} required id="conference" name="conference" className="form-select result">
                      <option value="">Choose a conference</option>
                      {this.state.conferences.map(conference => {
                            return (
                            <option key={conference.id} value={conference.id}>
                                {conference.name}
                            </option>
                            );
                        })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
}

export default PresentationForm;