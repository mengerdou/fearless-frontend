import React from 'react';

class ConferenceForm extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations:'',
            max_attendees:'',
            locations:[]

        };

        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        
        
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

        const cleared = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations:'',
            max_attendees:'',
            location:'',
          };
        this.setState(cleared);

    }
}

handleFieldChange(event) {
    const value = event.target.value;
    this.setState({[event.target.id]: value})
}

    

    async componentDidMount(){
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        this.setState({locations: data.locations});
        
        
    
        // const selectTag = document.getElementById('location');
        // for (let location of data.locations) {
        //     let option = document.createElement("option");
        //     option.value = location.id
        //     option.innerHTML = location.name
        //     selectTag.appendChild(option)


        // }

    }}





    render(){
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFieldChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFieldChange} value={this.state.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                    <label htmlFor="starts">Starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFieldChange} value={this.state.ends} placeholder="ends" required type="date" name="ends" id="ends" className="form-control"/>
                    <label htmlFor="ends">Ends</label>
                  </div>
                  <div className="form-floating mb-3">
                    <textarea onChange={this.handleFieldChange} value={this.state.description} placeholder="description" className="form-control" id="description" name="description" id="description" rows="3"></textarea>
                    <label htmlFor="description">Description</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFieldChange} value={this.state.max_presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                    <label htmlFor="max_presentations">Maximum presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFieldChange} value={this.state.max_attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                    <label htmlFor="max_attendees">Maximum attendees</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleFieldChange} value={this.state.location} required id="location" name="location" className="form-select">
                      <option value="">Choose a location</option>
                      {this.state.locations.map(location => {
                            return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                            );
                        })}

                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>

        )
    }
}

export default ConferenceForm