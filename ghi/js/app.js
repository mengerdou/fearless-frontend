function createCard(name, description, pictureUrl, date, location) {
  return `
    <div class='col'>
      <div class="card shadow p-3 mb-5 bg-body rounded"> 
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle mb-2 text-muted">${location}</p> 
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">${date}</small>
        </div>
      </div>
      
    </div>
    `;
}


function errorhandleA() {
  return `
    <div class="alert alert-warning" role="alert">
    A simple warning alert—check it out!
    </div>
    `
}

function errorhandleB() {
  return `
        <div class="alert alert-info" role="alert">
        A simple info alert—check it out!
        </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);
    if (!response.ok) {
      const notice = errorhandleA();
      const aaa = document.querySelector("main");
      aaa.innerHTML = notice;

    } else {
      const data = await response.json();
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = new Date(details.conference.starts).toLocaleDateString();
          const ends = new Date(details.conference.ends).toLocaleDateString();
          const date = starts + "-" + ends;
          const location = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, date, location);
          const row = document.querySelector('.row');
          row.innerHTML += html;
        }
      }

    }
  } catch (e) {
    const notice = errorhandleB();
    const aaa = document.querySelector("main");
    aaa.innerHTML = notice;
  }

});







